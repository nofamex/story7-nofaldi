from django.db import models

# Create your models here.
class Person(models.Model):
    nama = models.CharField(max_length = 30)
    pesan = models.CharField(max_length = 100)
    warna = models.CharField(max_length = 30, default = "black")

    def __str__(self):
        return self.nama