from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('<id>', views.change, name='color'),
    path('confirm/', views.formed, name='form'),
    path('confirm/<id>', views.novalid, name='formdelete'),
]