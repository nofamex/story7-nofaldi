from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
from accordion import views as acv
from story9 import views as snv
from story10 import views as stv
from .models import Person
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class UnitTest(TestCase):

    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_using_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_using_func(self):
        response = Client().get('/')
        self.assertEqual(response.resolver_match.func, views.homepage)


    def test_model(self):
        new1 = Person.objects.create(nama="nofaldi",pesan="jangan galau terus dong")
        self.assertEqual(Person.objects.all().count(),1)
        self.assertEqual(new1.nama,str(new1))
    
    def test_form_input(self):
        response = self.client.post('/confirm/',{'name':'nofaldi','pesan':'jangan galau',})
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'konfirmasi.html')
        self.assertEqual(Person.objects.all().count(),1)
        self.assertEqual(response.resolver_match.func,views.formed)

    def test_confirm(self):
        new_obj = Person.objects.create(nama="nofaldi",pesan="jangan galau terus", id=3)
        response = self.client.get(reverse('home:formdelete', kwargs={'id':new_obj.id}))
        self.assertEqual(response.resolver_match.func,views.novalid)
        self.assertEqual(response.status_code,302)
        self.assertEqual(Person.objects.all().count(),0)
        self.assertEqual(response['location'],'/')

    def test_color_change(self):
        new_obj = Person.objects.create(nama="nofaldi",pesan="jangan galau terus", id=3)
        response = self.client.get(reverse('home:color', kwargs={'id':new_obj.id}))
        self.assertEqual(response.resolver_match.func,views.change)
        self.assertEqual(response.status_code,302)
        self.assertEqual(response['location'],'/')
    
    def test_url_view_story8(self):
        response = self.client.get('/accord/')
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.resolver_match.func,acv.accord)
        self.assertTemplateUsed(response,'acc.html')

    def test_new_app_existence(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.resolver_match.func,snv.landing)
        self.assertTemplateUsed(response,'book.html')

    def test_story10_template(self):
        response = self.client.get('/story10/')
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.resolver_match.func, stv.loginpage)
        self.assertTemplateUsed(response, 'login.html')
        response2 = self.client.get('/story10/register')
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response2.resolver_match.func, stv.register)
        self.assertTemplateUsed(response2, 'register.html')

class FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_fill_form(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        name = selenium.find_element_by_name('name')
        pesan = selenium.find_element_by_name('pesan')
        submit = selenium.find_element_by_id('submit')
        name.send_keys("nofaldi")
        pesan.send_keys("ini pesan")
        time.sleep(5)
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        selenium.find_element_by_id('yesclicker').click()
        time.sleep(3)
        selenium.find_element_by_name('colorchange').click()
        time.sleep(3)
        selenium.get('http://localhost:8000/accord/')
        selenium.find_element_by_id('testedcard').click()
        time.sleep(3)
        selenium.find_element_by_id('testedupper').click()
        time.sleep(3)
        selenium.find_element_by_id('testeddowner').click()
        time.sleep(3)
        selenium.find_element_by_id('foto').click()
        selenium.get('http://localhost:8000/story9/')
        selenium.find_element_by_id('valued').send_keys('one piece')
        selenium.find_element_by_id('inputted').click()
        selenium.get('http://localhost:8000/story10/')
        selenium.find_element_by_id('signup').click()
        selenium.find_element_by_id('signin').click()
        selenium.find_element_by_name('username').send_keys('test')
        selenium.find_element_by_name('password').send_keys('n0f4ld1k03')
        selenium.find_element_by_id('login').click()
        
