from django.shortcuts import render, redirect
from .models import Person
import random
# Create your views here.
def homepage(request):
    objek = Person.objects.all()
    return render(request,'home.html',{'isinya' : objek})

def formed(request):
    if request.method == "POST":
        name = request.POST['name']
        message = request.POST['pesan']
        new_obj = Person.objects.create(nama = name, pesan = message)
        new_obj.save()

        return render(request,'konfirmasi.html',{'obj':new_obj})

def novalid(request,id):
    ent = Person.objects.get(id=id)
    ent.delete()
    return redirect('/')

def change(request,id):
    color = ['red','green','blue','yellow']
    newcol = color[random.randint(0,3)]
    ent = Person.objects.get(id=id)
    ent.warna = newcol
    ent.save()
    return redirect('/')
