from django.urls import path
from . import views

app_name = 'ajx'

urlpatterns = [
    path('', views.landing, name='landing'),
]