$(document).ready(function () {
    $(".inputted").click(function () {
        var srch = $("#valued").val();
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + srch,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response))
                for (let i = 0; i < json.items.length; i++) {
                    var table = "<tr><td class='name'>" + json.items[i].volumeInfo.title +
                        "</td><td>" + json.items[i].volumeInfo.publisher + "</td><td>" +
                        json.items[i].volumeInfo.authors + "</td><td>" +
                        "<span class='badge badge-dark' id='like'>Like</span></td></tr>";
                    $("#tblanswr").removeClass("tbl");
                    $(".answr").append(table);
                }
            }
        });
    });

    var likeStored = [];

    function addBook(book) {
        var ada = false;
        for (let i = 0; i < likeStored.length; i++) {
            if (likeStored[i].Title == book) {
                ada = true;
                likeStored[i].Like += 1;
                break;
            }
        }
        if (ada == false) {
            likeStored.push({
                Title: book,
                Like: 1
            });
        }
    }

    $(".answr").on("click", "span#like", function () {
        var bookName = $(this).parent().siblings(".name").html();
        addBook(bookName);
        $(this).css("background-color", "green");
    })

    var top5 = [];

    function getRank() {
        top5 = [];
        let copyArr = [...likeStored];
        console.log(copyArr);
        for (let index = 0; index < copyArr.length; index++) {
            for (let i = 0; i < copyArr.length; i++) {
                if (top5.some(book => book.Title == copyArr[i].Title)) {
                    continue;
                }
                let point = copyArr[i];
                for (let y = 0; y < copyArr.length; y++) {
                    if (top5.some(book => book.Title == copyArr[y].Title)) {
                        continue;
                    } else if (copyArr[y].Like > point.Like) {
                        point = copyArr[y]
                    }
                }
                if (top5.length != 5) {
                    top5.push(point);
                }
            }
        }
        var temp = ""
        top5.forEach(element => {
            temp += element.Title + " ,Likes: " + element.Like + "<br></br>";
        });
        return temp;
    }

    $("#modale").click(function (e) {
        e.preventDefault();
        $(this).next().find(".isibody").html(getRank());
    });
})