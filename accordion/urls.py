from django.urls import path
from . import views

app_name = 'accord'

urlpatterns = [
    path('', views.accord, name='homeaccord'),
]