$(document).ready(function () {
    $(".card-header").click(function () {
        if ($(this).next(".card-body").hasClass("active")) {
            $(this).next(".card-body").removeClass("active").slideUp();
        } else {
            $(".card .card-body").slideUp();
            $(this).next(".card-body").addClass("active").slideDown();
        }

    })
    $("i.upper").click(function () {
        var pre = $(this).parents(".card").prev().find(".carda").text();
        var mine = $(this).prev(".carda").text();
        var minecon = $(this).parent().next().find("p").text();
        var precon = $(this).parents(".card").prev().find("p").text();
        if (pre !== "") {
            $(this).parents(".card").prev().find(".carda").text(mine);
            $(this).parent().next().find("p").text(precon);
            $(this).prev(".carda").text(pre);
            $(this).parents(".card").prev().find("p").text(minecon);
        }
    })
    $("i.downer").click(function () {
        var mine = $(this).siblings(".carda").text();
        var minecon = $(this).parent().next().find("p").text();
        var nex = $(this).parents(".card").next().find(".carda").text();
        var nexcon = $(this).parents(".card").next().find("p").text();
        if (nex !== "") {
            $(this).siblings(".carda").text(nex);
            $(this).parents(".card").next().find(".carda").text(mine);
            $(this).parent().next().find("p").text(nexcon);
            $(this).parents(".card").next().find("p").text(minecon);
        }
    })
    $(".pinktext").click(function () {
        if ($(this).hasClass("gayed")) {
            $(this).removeClass("gayed").text("これは私のアコーディオンです");
        } else {
            $(this).addClass("gayed").text("To Change Theme Click Nezuko-Chan");
        }
    })
    $(".nezuco").click(function () {
        if ($(this).hasClass("mikuchan")) {
            $(this).attr("src", "/static/nezuko1.png").removeClass("mikuchan");
            $(".card .card-header").removeClass("miku").addClass("nezuko");
            $(".pinktext").css("color", "pink")
            $(".bg").css("background-image", "url('/static/sakura.png')");
        } else {
            $(this).attr("src", "/static/miku.png").addClass("mikuchan");
            $(".card .card-header").removeClass("nezuko").addClass("miku");
            $(".pinktext").css("color", "#0EEADF")
            $(".bg").css("background-image", "url('/static/mikuwal.jpg')");

        }
    })
})