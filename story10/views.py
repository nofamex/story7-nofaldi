from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def loginpage(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/story10/hello')

    return render(request,'login.html')

def register(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story10/')
    context = {'form' : form}
    return render(request,'register.html', context)

def message(request):
    if request.user.is_authenticated:
        return render(request,'hello.html')
    else:
        return redirect('/story10/')

def logoutpage(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('/story10/')
    else:
        return redirect('www.google.com/login dulu pak')