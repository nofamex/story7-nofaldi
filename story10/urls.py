from django.urls import path
from . import views

app_name = 'authentication'

urlpatterns = [
    path('', views.loginpage, name='login'),
    path('register', views.register, name='register'),
    path('hello', views.message, name='message'),
    path('logout', views.logoutpage, name='logout')
]